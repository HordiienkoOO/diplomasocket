//
// Created by miheev on 17/10/2020.
//

#pragma once

#include <jni.h>
#include <string>

std::string jstring2string(JNIEnv *env, jstring jStr);
jstring string2jstring(JNIEnv *env, const std::string& str);