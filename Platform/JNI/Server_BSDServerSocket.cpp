#include "Server_BSDServerSocket.h"
#include "BSDServerSocket.h"
#include "BSDSocketDelegate.h"
#include "AndroidSocketDelegate.h"
#include "StringConverter.h"

FBSD::SOCKET::ServerSocket* getServerSocket(long socketPtr)
{
    return (FBSD::SOCKET::ServerSocket*) socketPtr;
}

extern "C"
{

jint Java_Server_BSDServerSocket_portNative(JNIEnv *, jclass, jlong socketPtr)
{
    return getServerSocket(socketPtr)->port();
}

jboolean Java_Server_BSDServerSocket_isReadyNative(JNIEnv *, jclass, jlong socketPtr)
{
    return getServerSocket(socketPtr)->isReady();
}

jboolean Java_Server_BSDServerSocket_isRunningNative(JNIEnv *, jclass, jlong socketPtr)
{
    return getServerSocket(socketPtr)->isRunning();
}

void Java_Server_BSDServerSocket_startSocketNative(JNIEnv *, jclass, jlong socketPtr)
{
    getServerSocket(socketPtr)->startSocket();
}

void Java_Server_BSDServerSocket_stopSocketNative(JNIEnv *, jclass, jlong socketPtr)
{
    getServerSocket(socketPtr)->stopSocket();
    // TODO cahebu4: delete object if needed
}

jboolean Java_Server_BSDServerSocket_sendNative(JNIEnv *env, jclass, jlong socketPtr, jstring jdata, jint descriptor)
{
    auto data = jstring2string(env, jdata);
    return getServerSocket(socketPtr)->sendData(data.c_str(), descriptor);
}

jlongArray Java_Server_BSDServerSocket_acceptedSocketsNative(JNIEnv *env, jclass, jlong socketPtr)
{
    auto socket = getServerSocket(socketPtr);
    auto acceptedSockets = socket->getAcceptedSockets();
    int size = acceptedSockets.size();

    jlongArray acceptedSocketsNative = env->NewLongArray(size);
    if (acceptedSocketsNative == NULL)
    {
        // Out of memory
        return NULL;
    }

    jlong fill[size];
    for (int i = 0; i < size; i++)
    {
        fill[i] = (long) acceptedSockets[i];
    }

    env->SetLongArrayRegion(acceptedSocketsNative, 0, size, fill);

    return acceptedSocketsNative;
}

jlong Java_Server_BSDServerSocket_ndkInit(JNIEnv *env, jclass, jint port, jobject javaDelegate)
{
    auto androidDelegate = new AndroidBSDSocketDelegate(env, javaDelegate);
    auto cDelegate = new FBSD::SOCKET::SocketDelegate(androidDelegate);
    return (long) new FBSD::SOCKET::ServerSocket(port, cDelegate);
}


}
