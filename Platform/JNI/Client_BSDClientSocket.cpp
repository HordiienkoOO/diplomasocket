#include "Client_BSDClientSocket.h"
#include "BSDClientSocket.h"
#include "BSDSocketDelegate.h"
#include "StringConverter.h"
#include "AndroidSocketDelegate.h"

FBSD::SOCKET::ClientSocket* getClientSocket(long socketPtr)
{
    return (FBSD::SOCKET::ClientSocket*) socketPtr;
}

extern "C"
{

jint Java_Client_BSDClientSocket_portNative(JNIEnv *, jclass, jlong socketPtr)
{
    return getClientSocket(socketPtr)->port();
}

jstring Java_Client_BSDClientSocket_addressNative(JNIEnv *env, jclass, jlong socketPtr)
{
    auto addr = getClientSocket(socketPtr)->address();
    return string2jstring(env, addr);
}

jboolean Java_Client_BSDClientSocket_isReadyNative(JNIEnv *, jclass, jlong socketPtr)
{
    return getClientSocket(socketPtr)->isReady();
}

jboolean Java_Client_BSDClientSocket_isRunningNative(JNIEnv *, jclass, jlong socketPtr)
{
    return getClientSocket(socketPtr)->isRunning();
}

void Java_Client_BSDClientSocket_startSocketNative(JNIEnv *, jclass, jlong socketPtr)
{
    getClientSocket(socketPtr)->startSocket();
}

void Java_Client_BSDClientSocket_stopSocketNative(JNIEnv *, jclass, jlong socketPtr)
{
    getClientSocket(socketPtr)->stopSocket();
    // TODO cahebu4: delete object if needed
}

jboolean Java_Client_BSDClientSocket_sendNative(JNIEnv *env, jclass, jlong socketPtr, jstring jdata)
{
    auto data = jstring2string(env, jdata);
    return getClientSocket(socketPtr)->sendData(data.c_str());
}

jlong Java_Client_BSDClientSocket_ndkInit(JNIEnv *env, jclass, jstring addr, jint port, jobject javaDelegate)
{
    auto androidDelegate = new AndroidBSDSocketDelegate(env, javaDelegate);
    auto cDelegate = new FBSD::SOCKET::SocketDelegate(androidDelegate);
    auto addrStr = jstring2string(env, addr);
    return (long) new FBSD::SOCKET::ClientSocket(addrStr, port, cDelegate);
}

}
