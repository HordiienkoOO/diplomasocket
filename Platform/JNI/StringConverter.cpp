#include "StringConverter.h"

std::string jstring2string(JNIEnv *env, jstring jStr)
{
    const char *cstr = env->GetStringUTFChars(jStr, NULL);
    std::string str = std::string(cstr);
    env->ReleaseStringUTFChars(jStr, cstr);
    return str;
}

jstring string2jstring(JNIEnv *env, const std::string& str)
{
    return env->NewStringUTF(str.c_str());
}
