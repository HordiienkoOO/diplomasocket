#include "DelegationManagement_BSDSigningManager.h"
#include "BSDSigningManager.h"

#include "StringConverter.h"
extern "C"
{
JNIEXPORT void JNICALL Java_DelegationManagement_BSDSigningManager_setCertificatesPath(JNIEnv *env, jclass, jstring jpath)
{
    auto path = jstring2string(env, jpath);
    FBSD::MANAGEMENT::SigningManager::sharedInstance()->setKeysFolder(path.c_str());
}

JNIEXPORT void JNICALL
Java_DelegationManagement_BSDSigningManager_configure(JNIEnv *env, jclass, jstring jcountry, jstring jstate, jstring jlocation,
                                                      jstring jorganization, jstring jorganizationUnit, jstring jcommonName, jstring jemailAddress)
{
    auto country = jstring2string(env, jcountry);
    auto state = jstring2string(env, jstate);
    auto location = jstring2string(env, jlocation);
    auto organization = jstring2string(env, jorganization);
    auto organizationUnit = jstring2string(env, jorganizationUnit);
    auto commonName = jstring2string(env, jcommonName);
    auto emailAddress = jstring2string(env, jemailAddress);
    FBSD::MANAGEMENT::SigningManager::sharedInstance()->configure(country.c_str(),
                                                               state.c_str(),
                                                               location.c_str(),
                                                               organization.c_str(),
                                                               organizationUnit.c_str(),
                                                               commonName.c_str(),
                                                               emailAddress.c_str());
}

}
