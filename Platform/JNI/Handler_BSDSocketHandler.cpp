#include "Handler_BSDSocketHandler.h"
#include "BSDSocketHandler.h"
#include "StringConverter.h"

FBSD::SOCKET::SocketHandler* getHandler(long handlerPtr)
{
    return (FBSD::SOCKET::SocketHandler*) handlerPtr;
}

extern "C"
{
jboolean Java_Handler_BSDSocketHandler_isHandlingNative(JNIEnv *, jclass, jlong handlerPtr)
{
    return getHandler(handlerPtr)->isHandling();
}

jint Java_Handler_BSDSocketHandler_descriptorNative(JNIEnv *, jclass, jlong handlerPtr)
{
    return getHandler(handlerPtr)->descriptor();
}

void Java_Handler_BSDSocketHandler_stopHandlingNative(JNIEnv *, jclass, jlong handlerPtr)
{
    getHandler(handlerPtr)->stopHandling();
}

jboolean Java_Handler_BSDSocketHandler_sendNative(JNIEnv *env, jclass, jlong handlerPtr, jstring jdata) {
    auto data = jstring2string(env, jdata);
    return getHandler(handlerPtr)->send(data.c_str());
}

}



