#include "AndroidSocketDelegate.h"
#include "BSDSocketDelegate.h"
#include "StringConverter.h"
#include "JNIListener.h"

#include <thread>

#pragma mark C Delegate

FBSD::SOCKET::SocketDelegate::SocketDelegate(void *androidDelegate)
{
    platformDelegate = androidDelegate;
}

void FBSD::SOCKET::SocketDelegate::didReceiveMessage(char *message, int descriptor)
{
    auto androidDelegate = (AndroidBSDSocketDelegate*) platformDelegate;
    std::string messageStr(message);
    androidDelegate->onMessageReceived(messageStr, descriptor);
}

void FBSD::SOCKET::SocketDelegate::didConnect(int descriptor)
{
    auto androidDelegate = (AndroidBSDSocketDelegate*) platformDelegate;
    androidDelegate->onConnect(descriptor);
}

FBSD::SOCKET::SocketDelegate::~SocketDelegate()
{
    auto androidDelegate = (AndroidBSDSocketDelegate*) platformDelegate;
    delete androidDelegate;
}

#pragma mark Android delegate

AndroidBSDSocketDelegate::AndroidBSDSocketDelegate(JNIEnv *env, jobject javaObject)
{
    javaObjectRef = env->NewGlobalRef(javaObject);
    jclass javaClass = env->GetObjectClass(javaObject);
    callbackConnenctMethodId = env->GetMethodID(javaClass, "onConnect", "(I)V");
    callbackMethodId = env->GetMethodID(javaClass, "onReceive", "(Ljava/lang/String;I)V");
}

AndroidBSDSocketDelegate::~AndroidBSDSocketDelegate()
{
    NDKLock lock;
    lock.getJNIEnv()->DeleteGlobalRef(javaObjectRef);
    javaObjectRef = nullptr;
    callbackMethodId = nullptr;
    callbackConnenctMethodId = nullptr;
}

void AndroidBSDSocketDelegate::onMessageReceived(const std::string& message, int descriptor)
{
    NDKLock lock;
    JNIEnv* env = lock.getJNIEnv();
    jstring jmessage = string2jstring(lock.getJNIEnv(), message);
    env->CallVoidMethod(javaObjectRef, callbackMethodId, jmessage, (jint) descriptor);
}

void AndroidBSDSocketDelegate::onConnect(int descriptor)
{
    NDKLock lock;
    JNIEnv* env = lock.getJNIEnv();
    env->CallVoidMethod(javaObjectRef, callbackConnenctMethodId, (jint) descriptor);

}
