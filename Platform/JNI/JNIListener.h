//
// Created by miheev on 17/10/2020.
//

#pragma once

#include <jni.h>

jint JNI_OnLoad(JavaVM *vm, void *reserved);
JavaVM* getJvm();

class NDKLock
{
public:
    NDKLock();
    JNIEnv* getJNIEnv();
    ~NDKLock();
private:
    JNIEnv* env;
    bool attached;
};
