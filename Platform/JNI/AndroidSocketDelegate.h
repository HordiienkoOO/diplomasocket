//
// Created by mihee on 17/10/2020.
//

#pragma once

#include <jni.h>
#include <string>

class AndroidBSDSocketDelegate
{
public:
    AndroidBSDSocketDelegate(JNIEnv*, jobject);
    ~AndroidBSDSocketDelegate();

public:
    void onMessageReceived(const std::string& message, int descriptor);
    void onConnect(int descriptor);

private:
    jmethodID callbackConnenctMethodId;
    jmethodID callbackMethodId;
    jobject javaObjectRef;
};


