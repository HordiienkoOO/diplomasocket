#include "JNIListener.h"

static JavaVM* jvm = nullptr;

jint JNI_OnLoad(JavaVM *vm, void *reserved)
{
    jvm = vm;
    return JNI_VERSION_1_6;
}

JavaVM *getJvm()
{
    return jvm;
}

NDKLock::NDKLock() {
    attached = false;
    env = nullptr;
    int res = getJvm()->GetEnv((void **)&env, JNI_VERSION_1_6);
    if (res == JNI_EDETACHED)
    {
        attached = getJvm()->AttachCurrentThread(&env, nullptr) == 0;
    }
}

JNIEnv *NDKLock::getJNIEnv()
{
    return env;
}

NDKLock::~NDKLock() {
    if (attached)
    {
        getJvm()->DetachCurrentThread();
    }
    env = nullptr;
}
