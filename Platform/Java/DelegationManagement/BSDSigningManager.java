/*
 * (c) 2020 cahebu4. All rights reserved.
 */
package DelegationManagement;

public class BSDSigningManager {

    public static native void setCertificatesPath(String path);

    public static native void configure(
        String country, 
        String state,
        String location, 
        String organization, 
        String organizationUnit,
        String commonName, 
        String email);
}