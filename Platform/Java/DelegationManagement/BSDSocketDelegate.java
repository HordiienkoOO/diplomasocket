/*
 * (c) 2020 cahebu4. All rights reserved.
 */

package DelegationManagement;

public interface BSDSocketDelegate {

    void onReceive(String message, int descriptor);

    void onConnect(int descriptor);

}