/*
 * (c) 2020 cahebu4. All rights reserved.
 */
package Server;

import java.util.LinkedList;
import java.util.List;

import DelegationManagement.BSDSocketDelegate;
import Handler.BSDSocketHandler;

public class BSDServerSocket {

    private long socketPtr;
    private BSDSocketDelegate delegate;

    public int port() {
        return portNative(socketPtr);
    }

    public boolean isReady() {
        return isReadyNative(socketPtr);
    }

    public boolean isRunning() {
        return isRunningNative(socketPtr);
    }

    public void startSocket() {
        startSocketNative(socketPtr);
    }

    public void stopSocket() {
        stopSocketNative(socketPtr);
    }

    public boolean send(String data, int descriptor) {
        return sendNative(socketPtr, data, descriptor);
    }

    public BSDSocketDelegate getDelegate()
    {
        return this.delegate;
    }

    public List<BSDSocketHandler> acceptedSockets()
    {
        long[] nativeHandlers = acceptedSocketsNative(socketPtr);

        List<BSDSocketHandler> acceptedSockets = new LinkedList<>();
        for (long nativeHandler : nativeHandlers) {
            acceptedSockets.add(new BSDSocketHandler(nativeHandler));
        }

        return acceptedSockets;
    }

    private static native int portNative(long socketPtr);
    private static native boolean isReadyNative(long socketPtr);
    private static native boolean isRunningNative(long socketPtr);
    private static native void startSocketNative(long socketPtr);
    private static native void stopSocketNative(long socketPtr);
    private static native boolean sendNative(long socketPtr, String data, int descriptor);
    private static native long[] acceptedSocketsNative(long socketPtr);

    private static native long ndkInit(int port, BSDSocketDelegate delegate);

    public BSDServerSocket(int port, BSDSocketDelegate delegate)
    {
        this.delegate = delegate;
        this.socketPtr = ndkInit(port, delegate);
    }

}