/*
 * (c) 2020 cahebu4. All rights reserved.
 */
package Client;

import DelegationManagement.BSDSocketDelegate;

public class BSDClientSocket {

    private long socketPtr;
    private BSDSocketDelegate delegate;

    public int port() {
        return portNative(socketPtr);
    }

    public String address() {
        return addressNative(socketPtr);
    }

    public boolean isReady() {
        return isReadyNative(socketPtr);
    }

    public boolean isRunning() {
        return isRunningNative(socketPtr);
    }

    public void startSocket() {
        startSocketNative(socketPtr);
    }

    public void stopSocket() {
        stopSocketNative(socketPtr);
    }

    public boolean send(String data) {
        return sendNative(socketPtr, data);
    }

    public BSDSocketDelegate getDelegate()
    {
        return this.delegate;
    }

    private static native int portNative(long socketPtr);
    private static native String addressNative(long socketPtr);
    private static native boolean isReadyNative(long socketPtr);
    private static native boolean isRunningNative(long socketPtr);
    private static native void startSocketNative(long socketPtr);
    private static native void stopSocketNative(long socketPtr);
    private static native boolean sendNative(long socketPtr, String data);

    private static native long ndkInit(String address, int port, BSDSocketDelegate delegate);

    public BSDClientSocket(String address, int port, BSDSocketDelegate delegate)
    {
        this.delegate = delegate;
        this.socketPtr = ndkInit(address, port, delegate);
    }

}