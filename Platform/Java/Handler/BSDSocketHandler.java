/*
 * (c) 2020 cahebu4. All rights reserved.
 */
package Handler;

public class BSDSocketHandler {

	private long handlerPtr;

	public boolean isHandling() {
		return isHandlingNative(handlerPtr);
	}

	public int descriptor() {
		return descriptorNative(handlerPtr);
	}

	public void stopHandling() {
		stopHandlingNative(handlerPtr);
	}

	public boolean send(String data) {
		return sendNative(handlerPtr, data);
	}

	private static native boolean isHandlingNative(long handlerPtr);
	private static native int descriptorNative(long handlerPtr);
	private static native void stopHandlingNative(long handlerPtr);
	private static native boolean sendNative(long handlerPtr, String data);

	public BSDSocketHandler(long handlerPtr) {
		this.handlerPtr = handlerPtr;
	}

}
