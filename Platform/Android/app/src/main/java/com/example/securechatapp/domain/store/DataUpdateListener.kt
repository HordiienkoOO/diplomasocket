package com.example.securechatapp.domain.store

import com.example.securechatapp.domain.message.Message

interface DataUpdateListener {
    fun onUpdate(data: List<Message>)
}