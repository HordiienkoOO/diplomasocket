package com.example.securechatapp.domain.message

enum class MessageType {

    CONNECT,
    DISCONNECT,
    STRING

}