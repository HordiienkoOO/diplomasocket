package com.example.securechatapp.domain.message

import org.json.JSONObject

object MessageFactory {

    fun createMessage(json: JSONObject): Message {
        val typeInt = json.getInt(Message.TYPE_KEY)
        val type: MessageType = MessageType.values()[typeInt]
        val username: String = json.getJSONObject(Message.USER_KEY).getString(Message.NICKNAME_KEY)
        return when(type) {
            MessageType.CONNECT -> ConnectMessage(username)
            MessageType.DISCONNECT -> DisconnectMessage(username)
            MessageType.STRING -> {
                val content = json.getString(StringMessage.CONTENT_KEY)
                StringMessage(username, content)
            }
        }
    }
}