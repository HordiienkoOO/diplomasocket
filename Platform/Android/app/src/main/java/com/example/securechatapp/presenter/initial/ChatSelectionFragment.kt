package com.example.securechatapp.presenter.initial

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.example.securechatapp.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.chat_selection_fragment.view.*

@AndroidEntryPoint
class ChatSelectionFragment : Fragment() {

    companion object {
        fun newInstance() = ChatSelectionFragment()
    }

    private val viewModel: ChatSelectionViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.chat_selection_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.username.observe(viewLifecycleOwner, {
            view.welcomeTitleTV.text = getString(R.string.welcome_title, it)
        })
        view.createChatBtn.setOnClickListener {
            it.isClickable = false
            viewModel.createServer()
        }
        view.joinChatBtn.setOnClickListener {
            it.isClickable = false
            viewModel.joinServer()
        }
        viewModel.canJoin.observe(viewLifecycleOwner, {
            if (it) {
                val action = ChatSelectionFragmentDirections.actionOpenChat()
                view.findNavController().navigate(action)
            }
        })
    }
}