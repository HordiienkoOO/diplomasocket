package com.example.securechatapp.presenter.initial

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.securechatapp.R
import com.example.securechatapp.domain.session.ActiveSession
import com.example.securechatapp.domain.session.ActiveSessionHolder


class SignUpViewModel @ViewModelInject constructor(private val activeSessionHolder: ActiveSessionHolder) : ViewModel() {

    private val _usernameValidationResult = MutableLiveData<Int>()
    val usernameValidationResult: LiveData<Int> = _usernameValidationResult
    val successCode = 0

    fun signUp(username: String) {
        if (username.isEmpty()) {
            _usernameValidationResult.postValue(R.string.username_empty_error)
        } else {
            activeSessionHolder.activeSession = ActiveSession(username)
            _usernameValidationResult.postValue(successCode)
        }
    }
}