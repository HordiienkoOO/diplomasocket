package com.example.securechatapp.presenter.initial

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.example.securechatapp.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.sign_up_fragment.view.*

@AndroidEntryPoint
class SignUpFragment : Fragment() {

    companion object {
        fun newInstance() = SignUpFragment()
    }

    private val viewModel: SignUpViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.sign_up_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.usernameValidationResult.observe(this.viewLifecycleOwner, { result ->
            if (result == viewModel.successCode) {
                val action = SignUpFragmentDirections.actionSignUp()
                view.findNavController().navigate(action)
            } else {
                view.userNameTv.error = getString(result)
            }
        })
        view.signUpBtn.setOnClickListener {
            viewModel.signUp(view.userNameTv.text.toString())
        }
    }
}