package com.example.securechatapp.domain.store

import com.example.securechatapp.domain.connection.MessageListener
import com.example.securechatapp.domain.message.Message
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.lang.ref.WeakReference

class MessageStore: MessageListener {

    private val messages = mutableListOf<Message>()
    private val listeners = mutableListOf<WeakReference<DataUpdateListener>>()
    private val mutex = Mutex()

    fun subscribe(listener: DataUpdateListener) {
        listeners.add(WeakReference(listener))
    }

    override suspend fun onMessageReceived(message: Message) {
        mutex.withLock {
            messages.add(message)
        }

        notifyAll()
    }

    private suspend fun notifyAll() {
        mutex.withLock {
            listeners.forEach {
                it.get()?.onUpdate(messages)
            }
        }
    }
}