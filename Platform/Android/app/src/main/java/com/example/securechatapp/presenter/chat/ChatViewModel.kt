package com.example.securechatapp.presenter.chat

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.securechatapp.domain.connection.Connection
import com.example.securechatapp.domain.message.Message
import com.example.securechatapp.domain.session.ActiveSession
import com.example.securechatapp.domain.store.DataUpdateListener
import com.example.securechatapp.domain.store.MessageStore
import kotlinx.coroutines.launch

class ChatViewModel @ViewModelInject constructor(private val connection: Connection,
                                                 messageStore: MessageStore,
                                                 val activeSession: ActiveSession)
                                                    : ViewModel(), DataUpdateListener {

    init {
        messageStore.subscribe(this)
    }

    private val _data = MutableLiveData<List<Message>>()
    val data: LiveData<List<Message>> = _data

    override fun onUpdate(data: List<Message>) {
        _data.postValue(data)
    }

    fun sendMessage(message: String) {
        viewModelScope.launch {
            connection.sendMessage(message)
        }
    }

}