package com.example.securechatapp.presenter.initial

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.securechatapp.data.ClientHolder
import com.example.securechatapp.data.ConnectionFactory
import com.example.securechatapp.data.ServerHolder
import com.example.securechatapp.domain.session.ActiveSession
import com.example.securechatapp.domain.store.MessageStore
import kotlinx.coroutines.launch

class ChatSelectionViewModel @ViewModelInject constructor(private val activeSession: ActiveSession,
                                                            private val serverHolder: ServerHolder,
                                                            private val clientHolder: ClientHolder,
                                                            private val messageStore: MessageStore) : ViewModel() {


    private val _canJoin = MutableLiveData<Boolean>()
    val canJoin: LiveData<Boolean> = _canJoin

    private val _username = MutableLiveData(activeSession.username)
    val username: LiveData<String> = _username

    fun createServer() {
        viewModelScope.launch {
            serverHolder.serverSocket = ConnectionFactory.createServer()
            join()
        }
    }

    fun joinServer() {
        viewModelScope.launch {
            join()
        }
    }

    private suspend fun join() {
        clientHolder.clientSocket = ConnectionFactory.createClient(activeSession, messageStore)
        _canJoin.postValue(true)
    }
}