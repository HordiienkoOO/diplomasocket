package com.example.securechatapp.data

import Client.BSDClientSocket
import DelegationManagement.BSDSocketDelegate
import Server.BSDServerSocket
import com.example.securechatapp.domain.connection.MessageListener
import com.example.securechatapp.domain.message.ConnectMessage
import com.example.securechatapp.domain.message.MessageFactory
import com.example.securechatapp.domain.session.ActiveSession
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject

object ConnectionFactory {

    suspend fun createServer(): BSDServerSocket = withContext(Dispatchers.IO) {
        lateinit var socket: BSDServerSocket

        socket = BSDServerSocket(PORT, object:BSDSocketDelegate {
            override fun onReceive(message: String?, descriptor: Int) {
                socket.acceptedSockets().forEach {
                    it.send(message)
                }
            }

            override fun onConnect(descriptor: Int) {
            }

        })
        socket.startSocket()
        return@withContext socket
    }

    suspend fun createClient(activeSession: ActiveSession, delegate: MessageListener): BSDClientSocket = withContext(Dispatchers.IO) {
        lateinit var socket: BSDClientSocket
        socket = BSDClientSocket(IP, PORT_CLIENT, object: BSDSocketDelegate {
            override fun onReceive(message: String?, descriptor: Int) {
                if (message != null) {
                    val json = JSONObject(message)
                    val msg = MessageFactory.createMessage(json)
                    GlobalScope.launch {
                        withContext(Dispatchers.IO) {
                            delegate.onMessageReceived(msg)
                        }
                    }
                }
            }

            override fun onConnect(descriptor: Int) {
                val connectMessage = ConnectMessage(activeSession.username)
                socket.send(connectMessage.toString())
            }
        })
        socket.startSocket()
        return@withContext socket
    }

    private const val PORT = 8080
    private const val PORT_CLIENT = 8080
    private const val IP = "127.0.0.1"


}