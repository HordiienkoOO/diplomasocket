package com.example.securechatapp.domain.session

data class ActiveSession(val username: String) {
}