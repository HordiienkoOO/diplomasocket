package com.example.securechatapp.presenter.chat

import android.view.Gravity
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.securechatapp.R
import com.example.securechatapp.domain.message.ConnectMessage
import com.example.securechatapp.domain.message.DisconnectMessage
import com.example.securechatapp.domain.message.Message
import com.example.securechatapp.domain.message.StringMessage
import com.example.securechatapp.domain.session.ActiveSession

/**
 * [RecyclerView.Adapter] that can display a [Message].
 * TODO: Replace the implementation with code for your data type.
 */
class MyChatItemRecyclerViewAdapter(
        private val activeSession: ActiveSession,
        private val values: List<Message>)
    : RecyclerView.Adapter<MyChatItemRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_chat_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when(val item = values[position]) {
            is ConnectMessage -> {
                holder.container.gravity = Gravity.CENTER_HORIZONTAL
                holder.setOnlyMessage("${item.username} joined the chat")
            }
            is DisconnectMessage -> {
                holder.container.gravity = Gravity.CENTER_HORIZONTAL
                holder.setOnlyMessage("${item.username} has left left the chat")
            }
            is StringMessage -> {
                if (item.username == activeSession.username) {
                    holder.container.gravity = Gravity.START
                } else {
                    holder.container.gravity = Gravity.END
                }
                holder.setAll(item.username, item.content)
            }
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val container: RelativeLayout = view.findViewById(R.id.messageContainer)
        private val usernameView: TextView = view.findViewById(R.id.userNameTv)
        private val contentView: TextView = view.findViewById(R.id.content)

        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }

        fun setOnlyMessage(text: String) {
            usernameView.visibility = View.GONE
            contentView.text = text
        }

        fun setAll(user: String, message: String) {
            usernameView.visibility = View.VISIBLE
            usernameView.text = user
            contentView.text = message
        }
    }
}