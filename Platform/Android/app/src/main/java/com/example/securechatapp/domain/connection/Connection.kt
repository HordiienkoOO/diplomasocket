package com.example.securechatapp.domain.connection

interface Connection {

    suspend fun sendMessage(message: String)

}