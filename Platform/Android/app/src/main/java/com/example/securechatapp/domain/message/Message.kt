package com.example.securechatapp.domain.message

import org.json.JSONObject

sealed class Message(val type: MessageType, val username: String) {
    open fun toJson(): JSONObject {
        val userObj = JSONObject()
        val finalObj = JSONObject()

        userObj.put(NICKNAME_KEY, username)
        finalObj.put(USER_KEY, userObj)
        finalObj.put(TYPE_KEY, type.ordinal)

        return finalObj
    }

    override fun toString(): String {
        return toJson().toString()
    }

    companion object {
        const val TYPE_KEY = "messageType"
        const val NICKNAME_KEY = "nickname"
        const val USER_KEY = "user"
    }
}

class ConnectMessage(username: String) : Message(MessageType.CONNECT, username)
class DisconnectMessage(username: String) : Message(MessageType.DISCONNECT, username)
class StringMessage(username: String, val content: String) : Message(MessageType.STRING, username) {
    override fun toJson(): JSONObject {
        val baseObj = super.toJson()
        baseObj.put(CONTENT_KEY, content)
        return baseObj
    }

    companion object {
        const val CONTENT_KEY = "content"
    }
}
