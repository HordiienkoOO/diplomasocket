package com.example.securechatapp.domain.connection

import com.example.securechatapp.domain.message.Message

interface MessageListener {

    suspend fun onMessageReceived(message: Message)

}