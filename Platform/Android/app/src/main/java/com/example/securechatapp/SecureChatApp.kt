package com.example.securechatapp

import DelegationManagement.BSDSigningManager
import android.app.Application
import com.example.socketlib.Initializer
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SecureChatApp: Application() {

    override fun onCreate() {
        super.onCreate()
        Initializer.initialize()
        BSDSigningManager.setCertificatesPath(filesDir.absolutePath)
        BSDSigningManager.configure("UA",
                "K",
                "Kyiv",
                "KPI",
                "Diploma",
                "TestCert",
                "test@email.com")
    }
}