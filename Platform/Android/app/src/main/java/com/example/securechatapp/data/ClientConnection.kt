package com.example.securechatapp.data

import Client.BSDClientSocket
import com.example.securechatapp.domain.connection.Connection
import com.example.securechatapp.domain.message.StringMessage
import com.example.securechatapp.domain.session.ActiveSession
import javax.inject.Inject

class ClientConnection @Inject constructor(private val clientSocket: BSDClientSocket, private val activeSession: ActiveSession): Connection {

     override suspend fun sendMessage(message: String) {
        val msg = StringMessage(activeSession.username, message)
        clientSocket.send(msg.toString())
    }
}