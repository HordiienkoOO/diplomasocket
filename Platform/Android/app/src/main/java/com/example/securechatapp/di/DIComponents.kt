package com.example.securechatapp.di

import com.example.securechatapp.data.ClientConnection
import com.example.securechatapp.data.ClientHolder
import com.example.securechatapp.data.ServerHolder
import com.example.securechatapp.domain.connection.Connection
import com.example.securechatapp.domain.session.ActiveSession
import com.example.securechatapp.domain.session.ActiveSessionHolder
import com.example.securechatapp.domain.store.MessageStore
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object DIComponents {

    @Provides
    @Singleton
    fun provideSessionHolder(): ActiveSessionHolder = ActiveSessionHolder()

    @Provides
    fun provideSession(activeSessionHolder: ActiveSessionHolder): ActiveSession = activeSessionHolder.activeSession

    @Provides
    @Singleton
    fun provideServerHolder(): ServerHolder = ServerHolder()

    @Provides
    @Singleton
    fun provideClientHolder(): ClientHolder = ClientHolder()

    @Provides
    fun provideConnection(clientHolder: ClientHolder, sessionHolder: ActiveSessionHolder): Connection = ClientConnection(clientHolder.clientSocket, sessionHolder.activeSession)

    @Provides
    @Singleton
    fun provideMessageStore(): MessageStore = MessageStore()
}