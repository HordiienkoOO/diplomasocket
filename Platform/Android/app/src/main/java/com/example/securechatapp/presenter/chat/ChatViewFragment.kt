package com.example.securechatapp.presenter.chat

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.example.securechatapp.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_chat_item_list.view.*

/**
 * A fragment representing a list of Items.
 */
@AndroidEntryPoint
class ChatViewFragment : Fragment() {

    private val viewModel: ChatViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_chat_item_list, container, false)

        // Set the adapter

        with(view.list) {
            layoutManager = LinearLayoutManager(context)
            viewModel.data.observe(viewLifecycleOwner, {
                adapter = MyChatItemRecyclerViewAdapter(viewModel.activeSession, it)
            })
        }

        view.sendBtn.setOnClickListener {
            viewModel.sendMessage(view.messageET.text.toString())
            view.messageET.text?.clear()
        }
        return view
    }
}