package com.example.socketlib;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import Client.BSDClientSocket;
import DelegationManagement.BSDSigningManager;
import DelegationManagement.BSDSocketDelegate;
import Server.BSDServerSocket;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SocketsInstrumentedTest {

    @BeforeClass
    public static void initNativeLibrary() {
        Initializer.initialize();

        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        String path = appContext.getFilesDir().getAbsolutePath();

        BSDSigningManager.setCertificatesPath(path);
        BSDSigningManager.configure("UA",
                "K",
                "Kyiv",
                "KPI",
                "Diploma",
                "TestCert",
                "test@email.com");
    }

    private static final String messageToSend = "Test message";
    private static final String address = "127.0.0.1";
    private static int port = 8080;

    private final ReentrantLock lock = new ReentrantLock();
    private final Condition messageReceived = lock.newCondition();

    private final BSDSocketDelegate serverDelegate = new BSDSocketDelegate() {
        @Override
        public void onReceive(String message, int descriptor) {
            assertEquals("Incorrect message received", messageToSend, message);
            lock.lock();
            messageReceived.signal();
            lock.unlock();
        }

        @Override
        public void onConnect(int descriptor) {

        }
    };

    private BSDServerSocket serverSocket;
    private BSDClientSocket clientSocket;
    private int expectedConnections = 0;

    @Test
    public void test_verifyClientServerCommunication() {
        startServer();
        connectClient();
        verifyServerConnections();

        sendMessageToServerAndVerify();

        //closeClient();
        //verifyServerConnections();
        //closeServer();
    }

    private void startServer() {
        serverSocket = new BSDServerSocket(port, serverDelegate);
        assertTrue("Server is not ready", serverSocket.isReady());
        serverSocket.startSocket();
        assertTrue("Server is not running", serverSocket.isRunning());
    }

    private void connectClient() {
        clientSocket = new BSDClientSocket(address, port, new BSDSocketDelegate() {
            @Override
            public void onReceive(String message, int descriptor) {

            }

            @Override
            public void onConnect(int descriptor) {

            }
        });
        assertTrue("Client is not ready", clientSocket.isReady());
        clientSocket.startSocket();
        expectedConnections++;
        assertTrue("Client is not running", clientSocket.isRunning());
    }

    private void verifyServerConnections() {
        try {
            // Wait for connection to be established
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int activeConnections = serverSocket.acceptedSockets().size();
        assertEquals("Connections issue", expectedConnections, activeConnections);
    }

    private void sendMessageToServerAndVerify() {
        boolean messageSent = clientSocket.send(messageToSend);
        boolean received = false;

        lock.lock();
        try {
            received = messageReceived.await(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }

        assertTrue("Message was not sent", messageSent);
        assertTrue("Message was not received", received);
    }

    private void closeServer() {
        serverSocket.stopSocket();
        assertFalse("Server is ready but it should be not", serverSocket.isReady());
        assertFalse("Server is running but it should be not", serverSocket.isRunning());
    }

    private void closeClient() {
        clientSocket.stopSocket();
        assertFalse("Client is ready but it should be not", clientSocket.isReady());
        assertFalse("Client is running but it should be not", clientSocket.isRunning());
        expectedConnections--;
    }
}