/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#import "BSDSocketDelegate.h"
#import "SSLSocketDelegate.h"
#import <Foundation/Foundation.h>

#pragma mark - Methods
void FBSD::SOCKET::SocketDelegate::didReceiveMessage(char *message, int descriptor) {
    NSStringEncoding encoding = [NSString defaultCStringEncoding];
    NSString *receivedMessage = [NSString stringWithCString:message encoding:encoding];
    [(__bridge id<SSLSocketDelegate>)(platformDelegate)didReceiveMessage:receivedMessage fromDescriptor:descriptor];
}

void FBSD::SOCKET::SocketDelegate::didConnect(int descriptor) {
    [(__bridge id<SSLSocketDelegate>)(platformDelegate)didConnectWithDescriptor:descriptor];
}

#pragma mark - Constructor
FBSD::SOCKET::SocketDelegate::SocketDelegate(void *objcDelegate)
{
    this->platformDelegate = objcDelegate;
}

#pragma mark - Destructor
FBSD::SOCKET::SocketDelegate::~SocketDelegate()
{
}
