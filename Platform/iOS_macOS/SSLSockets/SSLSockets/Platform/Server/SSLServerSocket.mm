/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#import "BSDServerSocket.h"
#import "SSLServerSocket.h"
#import "BSDSocketDelegate.h"
#import "SSLSocketHandler+Protected.h"

@interface SSLServerSocket ()
@property (unsafe_unretained, nonatomic) FBSD::SOCKET::ServerSocket *socket;
@end

@implementation SSLServerSocket

- (int)port
{
    return self.socket->port();
}

- (BOOL)startSocket
{
    return self.socket->startSocket();
}

- (BOOL)isReady
{
    return self.socket->isReady();
}

- (BOOL)isRunning
{
    return self.socket->isRunning();
}

- (void)stopSocket
{
    self.socket->stopSocket();
}

- (id<SSLSocketDelegate>)delegate
{
    return (__bridge id<SSLSocketDelegate>)self.socket->delegate()->platformDelegate;
}

- (NSArray<SSLSocketHandler *> *)acceptedSockets
{
    NSMutableArray *acceptedSockets = [[NSMutableArray alloc] init];
    const std::vector<FBSD::SOCKET::SocketHandler *> _acceptedSockets = self.socket->getAcceptedSockets();
    for (FBSD::SOCKET::SocketHandler *handler : _acceptedSockets) {
        SSLSocketHandler *objcHandler = [[SSLSocketHandler alloc] initWithBSDSocketHandler:handler];
        [acceptedSockets addObject:objcHandler];
    }
    return acceptedSockets;
}

#pragma mark - Methods
- (BOOL)sendData:(NSString *)data toDescriptor:(NSInteger)descriptor
{
    const char *dataToSend = [data UTF8String];
    return self.socket->sendData(dataToSend, (int)descriptor);
}

#pragma mark - Constructors
- (instancetype)initWithPort:(int)port
{
    return [self initWithPort:port andDelegate:nil];
}

- (instancetype)initWithPort:(int)port andDelegate:(id <SSLSocketDelegate>)delegate
{
    self = [super init];
    if (self) {
        FBSD::SOCKET::SocketDelegate *cDelegate = new FBSD::SOCKET::SocketDelegate((__bridge void *)delegate);
        _socket = new FBSD::SOCKET::ServerSocket(port, cDelegate);
    }
    return self;
}

#pragma mark - Destructor
- (void)dealloc {
    delete self.socket;
}

@end
