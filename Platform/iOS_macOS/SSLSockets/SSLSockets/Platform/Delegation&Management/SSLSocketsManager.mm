/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#import "SSLSocketsManager.h"
#import "BSDSigningManager.h"

@implementation SSLSocketsManager

+ (void)setCertificatesPath:(NSString *)path
{
    FBSD::MANAGEMENT::SigningManager::sharedInstance()->setKeysFolder([path UTF8String]);
}

+ (void)configureCertificatesWithCountry:(NSString *)country
                       state:(NSString *)state
                    location:(NSString *)location
                organization:(NSString *)organization
            organizationUnit:(NSString *)organizationUnit
                  commonName:(NSString *)commonName
                emailAddress:(NSString *)emailAddress
{
    
    
    FBSD::MANAGEMENT::SigningManager::sharedInstance()->configure([country UTF8String],
                                                                  [state UTF8String],
                                                                  [location UTF8String],
                                                                  [organization UTF8String],
                                                                  [organizationUnit UTF8String],
                                                                  [commonName UTF8String],
                                                                  [emailAddress UTF8String]);
}


@end
