/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#import "SSLClientSocket.h"
#import "BSDClientSocket.h"
#import "BSDSocketDelegate.h"

@interface SSLClientSocket ()
@property (unsafe_unretained, nonatomic) FBSD::SOCKET::ClientSocket *socket;
@end

@implementation SSLClientSocket

#pragma mark - Getters
- (BOOL)isReady
{
    return self.socket->isReady();
}

- (BOOL)isRunning
{
    return self.socket->isRunning();
}

- (int)port
{
    return self.socket->port();
}

- (NSString *)address
{
    std::string _address = self.socket->address();
    const char* address = _address.c_str();
    NSStringEncoding encoding = [NSString defaultCStringEncoding];
    return [NSString stringWithCString:address encoding:encoding];
}

#pragma mark - Mathods

- (BOOL)sendData:(NSString *)data
{
    const char *dataToSend = [data UTF8String];
    return self.socket->sendData(dataToSend);
}

- (BOOL)startSocket
{
    return self.socket->startSocket();
}

- (void)stopSocket
{
    self.socket->stopSocket();
}

#pragma mark - Constructors

- (instancetype)initWithAddress:(NSString *)address port:(int)port andDelegate:(id <SSLSocketDelegate>)delegate {
    self = [super init];
    if (self) {
        std::string addr = std::string([address UTF8String]);
        FBSD::SOCKET::SocketDelegate *cDelegate = new FBSD::SOCKET::SocketDelegate((__bridge void *)delegate);
        _socket = new FBSD::SOCKET::ClientSocket(addr, port, cDelegate);
    }
    return self;
}

#pragma mark - Destructor
- (void)dealloc {
    delete _socket;
}

@end
