/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#import "SSLSocketHandler.h"
#include "BSDSocketHandler.h"

NS_ASSUME_NONNULL_BEGIN

@interface SSLSocketHandler ()

@property (unsafe_unretained, nonatomic) FBSD::SOCKET::SocketHandler *handler;

- (instancetype)initWithBSDSocketHandler:(FBSD::SOCKET::SocketHandler *)handler;

@end

NS_ASSUME_NONNULL_END
