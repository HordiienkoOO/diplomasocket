/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#import "SSLSocketHandler.h"
#import "SSLSocketHandler+Protected.h"

@implementation SSLSocketHandler

#pragma mark - Getters
- (id)ssl
{
    return (__bridge id)self.handler->getSSL();
}

#pragma mark - Methods
- (void)stopHandling
{
    self.handler->stopHandling();
}

- (BOOL)sendData:(NSString *)data {
    const char *dataToSend = [data UTF8String];
    return self.handler->send(dataToSend);
}

#pragma mark - Constructor
- (instancetype)initWithBSDSocketHandler:(FBSD::SOCKET::SocketHandler *)handler {
    self = [super init];
    if (self)
    {
        _handler = handler;
    }
    return self;
}

@end
