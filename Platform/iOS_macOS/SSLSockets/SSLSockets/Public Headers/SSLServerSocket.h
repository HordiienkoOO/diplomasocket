/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#import <SSLSockets/SSLSocketHandler.h>
#import <SSLSockets/SSLSocketDelegate.h>

NS_ASSUME_NONNULL_BEGIN

@interface SSLServerSocket : NSObject

/**
    @brief Socket's port.
 */
@property (assign, nonatomic, readonly) int port;

/**
    @brief Manages received messages. Notification will send to delegate when Socket will receive new message.
 */
@property (weak, nonatomic, readwrite) id <SSLSocketDelegate> delegate;

/**
    @brief Returns a Boolean value indicating configuring success.
    @discussion Value YES returns in case of successful configuring, value NO returns in case of any BSD Sockets API functions was failed. Means impossibility of current object using.
 */
@property (assign, nonatomic, readonly) BOOL isReady;

/**
    @brief Returns a Boolean value indicating state of socket.
    @discussion
        Value YES may be returned only in case Socket is successfully configured, already started and ready to send/receive messages.
 
        Value NO may be returned in following cases:
            * error occured while Socket configuring and that is why it cannot be runned (check isReady property);
            * socket was configured but not already started (use Start method to start it);
            * socket already closed: suspended state before destroying object.
 */
@property (assign, nonatomic, readonly) BOOL isRunning;

/**
    @brief Sorage that contains accepted sockets in SSLSocketHandler *representation.
 */
@property (strong, nonatomic, readonly) NSArray<SSLSocketHandler *> *acceptedSockets;

/**
    @brief Starts Socket, makess it ready to send/receive messages.
    @return YES if the receiver was successfully configured and being started or NO if it is not.
 */
- (BOOL)startSocket;

/**
    @brief Closes Socket and frees memory, makes it impossible to reuse.
 */
- (void)stopSocket;

/**
    @brief Sends given message to known accepted Sockets SSL.
    @param data - any information, converted to NSString *, that will be sent to SSLServerSocket.
    @param descriptor - descriptor of accepted SSLServerSocket that will receive given data.
    @return YES if message was successfully send or NO if Socket is not configured/not running (check isReady and isRunning properties).
 */
- (BOOL)sendData:(NSString *)data toDescriptor:(NSInteger)descriptor;

/**
    @brief Designated initializer, creates and configures SSLServerSocket.
    @discussion Creates and configures SSL Server Socket with given port and delegate.

    Configuring may be failed while using BSD Sockets API (calls: socket(), bind() and listen()). Errors will be displayed in console.
 
    @param port - number between 1 and 65535 which will be retained as Socket port.
    @param delegate - will manage received messages. Notification will send to delegate when Socket will receive new message.
    @return SSLServerSocket, configured with given port.
 */
- (instancetype)initWithPort:(int)port andDelegate:(nullable id <SSLSocketDelegate>)delegate;

@end

NS_ASSUME_NONNULL_END
