//
//  SSLSockets.h
//  SSLSockets
//
//  Created by Oleksandr Hordiienko on 1/22/19.
//  Copyright © 2019 SoftServe. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for SSLSockets.
FOUNDATION_EXPORT double SSLSocketsVersionNumber;

//! Project version string for SSLSockets.
FOUNDATION_EXPORT const unsigned char SSLSocketsVersionString[];

#import <SSLSockets/SSLServerSocket.h>
#import <SSLSockets/SSLClientSocket.h>
#import <SSLSockets/SSLSocketHandler.h>
#import <SSLSockets/SSLSocketDelegate.h>
#import <SSLSockets/SSLSocketsManager.h>


