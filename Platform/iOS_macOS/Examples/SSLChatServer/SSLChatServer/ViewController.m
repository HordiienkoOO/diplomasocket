//
//  ViewController.m
//  SSLChatServer
//
//  Created by Oleksandr on 19.10.2020.
//

#import "ViewController.h"
#import <SSLSockets/SSLServerSocket.h>

typedef NS_ENUM(NSInteger, SSLMessageType) {
    SSLMessageTypeConnect = 0,
    SSLMessageTypeDisconnect = 1,
    SSLMessageTypeStringMessage = 2,
    SSLMessageTypeP2PRequest = 3,
    SSLMessageTypeP2PResponse = 4
};
@interface ViewController () <SSLSocketDelegate>
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, strong) SSLServerSocket *socket;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.socket = [[SSLServerSocket alloc] initWithPort:8080 andDelegate:self];
    [self.socket startSocket];
    
    if ([self.socket isRunning])
    {
        self.textView.text = @"ServerSocket created";
    }
    else
    {
        self.textView.text = @"ServerSocket was not created";
    }
}

- (void)didReceiveMessage:(NSString *)message fromDescriptor:(NSInteger)descriptor
{
    NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    NSNumber *type = json[@"messageType"];
    SSLMessageType msgType = (SSLMessageType)[type integerValue];
    NSString *nickName = json[@"user"][@"nickname"];
    
    switch (msgType) {
        case SSLMessageTypeConnect:
        {
            [self logMessage:[NSString stringWithFormat:@"%@ has joined to the chat", nickName]];
            break;
        }
        case SSLMessageTypeDisconnect:
        {
            [self logMessage:[NSString stringWithFormat:@"%@ has disconnected from the chat", nickName]];
            break;
        }
        case SSLMessageTypeStringMessage:
        {
            [self logMessage:[NSString stringWithFormat:@"%@ has written something to the chat", nickName]];
            break;
        }
        default:
        {
            break;
        }
    }
    
    for (SSLSocketHandler *handler in self.socket.acceptedSockets)
    {
        [handler sendData:message];
    }
}

- (void)didConnectWithDescriptor:(NSInteger)descriptor
{
    
}

- (void)logMessage:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.textView.text = [self.textView.text stringByAppendingFormat:@"\n%@", message];
    });
}

@end
