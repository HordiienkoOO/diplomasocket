//
//  AppDelegate.m
//  SSLChatServer
//
//  Created by Oleksandr on 19.10.2020.
//

#import "AppDelegate.h"
#import <SSLSockets/SSLSockets.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
    [SSLSocketsManager setCertificatesPath:documentsPath];
    [SSLSocketsManager configureCertificatesWithCountry:@"UA"
                                                  state:@"Kyiv"
                                               location:@"Kyiv"
                                           organization:@"Igor Sikorsky KPI"
                                       organizationUnit:@"APEPS"
                                             commonName:@"APEPS"
                                           emailAddress:@"HordiienkoOO@gmail.com"];
    
    return YES;
}


#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}


@end
