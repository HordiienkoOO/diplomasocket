//
//  BSDLoginViewModel.swift
//  SSLChatClient
//
//  Created by Oleksandr on 18.10.2020.
//


typealias BSDLoginHandler = ((String) -> Void)

class BSDLoginViewModel {

    // MARK: - Properties
    private var handler: BSDLoginHandler
    
    // MARK: - Public methods
    func login(nickname: String) {
        handler(nickname)
    }
    
    // MARK: - Initializer
    init(handler: @escaping BSDLoginHandler) {
        self.handler = handler
    }
    
}
