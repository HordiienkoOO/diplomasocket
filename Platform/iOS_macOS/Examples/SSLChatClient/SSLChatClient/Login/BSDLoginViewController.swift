//
//  ViewController.swift
//  SSLChatClient
//
//  Created by Oleksandr on 18.10.2020.
//

import UIKit

class BSDLoginViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet private weak var nicknameField: UITextField!
    
    // MARK: - Properties
    private var viewModel: BSDLoginViewModel?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }

    // MARK: - Private mehtods
    private func setupController() {
        viewModel = BSDLoginViewModel(handler: loginHandler())
    }
    
    private func loginHandler() -> BSDLoginHandler {
        return { [weak self] (nickname) in
            guard let self = self else { return }
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let chatViewController = storyboard.instantiateViewController(withIdentifier: "BSDChatViewController") as! BSDChatViewController
            chatViewController.configure(nickname: nickname)
            self.navigationController?.pushViewController(chatViewController, animated: true)
        }
    }
    
    // MARK: - IBOutlets
    @IBAction private func enterButtonDidPress(_ sender: UIButton) {
        viewModel?.login(nickname: nicknameField.text ?? "Unknown")
    }
    
}

