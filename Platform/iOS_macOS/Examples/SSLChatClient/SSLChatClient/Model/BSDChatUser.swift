//
//  BSDChatUser.swift
//  SSLChatClient
//
//  Created by Oleksandr on 18.10.2020.
//

import UIKit

class BSDChatUser {

    // MARK: - Properties
    let nickname: String
    
    // MARK: - Public methods
    func toJSON() -> [String: String] {
        return ["nickname" : nickname]
    }
    
    // MARK: - ChatMessage
    convenience init(content: [String: Any]) {
        let nickname = content["nickname"] as! String
        self.init(nickname: nickname)
    }
    
    // MARK: - Intializers
    init(nickname: String) {
        self.nickname = nickname
    }
    
}
