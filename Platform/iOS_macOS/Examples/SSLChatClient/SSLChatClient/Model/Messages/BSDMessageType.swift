//
//  BSDMessageType.swift
//  SSLChatClient
//
//  Created by Oleksandr on 18.10.2020.
//

import Foundation

enum BSDMessageType: Int {
    
    case connect = 0
    case disconnect
    case stringMessage
    case p2pRequest
    
}

protocol BSDChatMessage {
    
    var messageType: BSDMessageType { get }
    
    init(content: [String: Any])
    
}
