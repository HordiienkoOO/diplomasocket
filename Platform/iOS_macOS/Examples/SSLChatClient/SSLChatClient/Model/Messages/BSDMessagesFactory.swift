//
//  BSDMessagesFactory.swift
//  SSLChatClient
//
//  Created by Oleksandr on 18.10.2020.
//

import UIKit

class BSDMessagesFactory {

    static func message(from content: String) -> BSDChatMessage? {
        let data = content.data(using: String.Encoding.utf8)!
        let msg = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
        let msgType = BSDMessageType(rawValue: msg["messageType"] as! Int)
        
        switch msgType {
            case .connect: return BSDConnectMessage(content: msg)
            case .disconnect: return BSDDisconnectMessage(content: msg)
            case .stringMessage: return BSDStringMessage(content: msg)
            default: return nil;
        }
        
    }
    
}
