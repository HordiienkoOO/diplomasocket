//
//  BSDConnectMessage.swift
//  SSLChatClient
//
//  Created by Oleksandr on 18.10.2020.
//

import UIKit

class BSDConnectMessage: BSDChatMessage {

    // MARK: - Properties
    let messageType = BSDMessageType.connect
    var user: BSDChatUser
    
    // MARK: - Public methods
    func toJSON() -> String {
        let dictionary = ["messageType": messageType.rawValue, "user": user.toJSON()] as [String : Any]
        let data = try! JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        return NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
    }
    
    // MARK: - BSDChatMessage
    required convenience init(content: [String: Any]) {
        let user = BSDChatUser(content: content["user"] as! [String : Any])
        self.init(user: user)
    }
    
    // MARK: - Initializers
    init(user: BSDChatUser) {
        self.user = user
    }
    
}
