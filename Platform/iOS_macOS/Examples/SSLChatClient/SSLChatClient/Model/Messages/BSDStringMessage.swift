//
//  BSDStringMessage.swift
//  SSLChatClient
//
//  Created by Oleksandr on 18.10.2020.
//

import UIKit

class BSDStringMessage: BSDChatMessage {

    // MARK: - Properties
    let messageType = BSDMessageType.stringMessage
    private(set) var user: BSDChatUser
    private(set) var message: String
    
    // MARK: - Public methods
    func toJSON() -> String {
        let dictionary = ["messageType": messageType.rawValue,
                          "user": user.toJSON(),
                          "content": message]
        as [String : Any]
        
        let data = try! JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        return NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
    }
    
    // MARK: - BSDChatMessage
    required convenience init(content: [String: Any]) {
        let user = BSDChatUser(content: content["user"] as! [String : Any])
        let message = content["content"] as! String
        self.init(user: user, message: message)
    }
    
    // MARK: - Initializers
    init(user: BSDChatUser, message: String) {
        self.user = user
        self.message = message
    }
    
}
