//
//  AppDelegate.swift
//  SSLChatClient
//
//  Created by Oleksandr on 18.10.2020.
//

import UIKit
import SSLSockets

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    internal var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        SSLSocketsManager.setCertificatesPath(documentsPath)
        SSLSocketsManager.configureCertificates(
            withCountry: "UA",
            state: "Kyiv",
            location: "Kyiv",
            organization: "Igor Sikorsky KPI",
            organizationUnit: "APEPS",
            commonName: "APEPS",
            emailAddress: "HordiienkoOO@gmail.com")
        
        return true
    }

}

