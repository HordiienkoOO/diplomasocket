//
//  BSDChatActionTableViewCell.swift
//  SSLChatClient
//
//  Created by Oleksandr on 18.10.2020.
//

import UIKit

class BSDChatActionTableViewCell: UITableViewCell {

    // MARK: - Constants
    static let identifier = "BSDChatActionTableViewCell"
    
    // MARK: - IBOutlets
    @IBOutlet private weak var actionLabel: UILabel!
    
    // MARK: - Public methdos
    func populateView(action: String) {
        actionLabel.text = action
    }
    
}
