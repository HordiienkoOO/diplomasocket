//
//  BSDMessageTableViewCell.swift
//  SSLChatClient
//
//  Created by Oleksandr on 18.10.2020.
//

import UIKit

class BSDMessageTableViewCell: UITableViewCell {

    // MARK: - Constants
    static let identifier = "BSDMessageTableViewCell"
    
    // MARK: - IBOutlets
    @IBOutlet private weak var authorLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    
    // MARK: - Public methdos
    func populateView(author: String, message: String) {
        authorLabel.text = author
        messageLabel.text = message
    }
    
}
