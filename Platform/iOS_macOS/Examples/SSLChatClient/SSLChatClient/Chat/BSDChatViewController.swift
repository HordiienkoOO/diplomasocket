//
//  BSDChatViewController.swift
//  SSLChatClient
//
//  Created by Oleksandr on 18.10.2020.
//

import UIKit

class BSDChatViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var messageField: UITextField!
    
    // MARK: - Properties
    private var viewModel: BSDChatViewModel?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    // MARK: - Public methods
    func configure(nickname: String) {
        viewModel = BSDChatViewModel(
            nickname: nickname,
            handleMessageAction: handleMessageAction())
    }
    
    // MARK: - Private methods
    private func setupController() {
        let identifiers = [BSDChatActionTableViewCell.identifier,
                           BSDMessageTableViewCell.identifier,
                           BSDMyMessageTableViewCell.identifier]
        
        for identifier in identifiers {
            let nib = UINib(nibName: identifier, bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: identifier)
        }
    }
    
    private func handleMessageAction() -> BSDHandleMessageAction {
        return { [weak self] in
            guard let self = self, self.tableView != nil else { return }
            self.tableView.reloadData()
        }
    }
    
    // MARK: - IBActions
    @IBAction private func sendButtonDidPress(_ sender: UIButton) {
        viewModel?.send(message: messageField.text ?? "")
        messageField.text = ""
    }

}

extension BSDChatViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellType = viewModel?.cell(for: indexPath.row) else { return UITableViewCell() }
        switch cellType {
            case .action(let action): return self.tableView(tableView, actionCellForRowAt: indexPath, withAction: action)
            case .message(let author, let message): return self.tableView(tableView, messageCellForRowAt: indexPath, with: author, and: message)
            case .myMessage(let author, let message): return self.tableView(tableView, myMessageCellForRowAt: indexPath, with: author, and: message)
        }
    }
    
    func tableView(_ tableView: UITableView, actionCellForRowAt indexPath: IndexPath, withAction action: String) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(
                withIdentifier: BSDChatActionTableViewCell.identifier,
                for: indexPath) as? BSDChatActionTableViewCell
        else { return UITableViewCell() }
        
        cell.populateView(action: action)
        return cell
    }
    
    func tableView(_ tableView: UITableView, messageCellForRowAt indexPath: IndexPath, with author: String, and message: String) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(
                withIdentifier: BSDMessageTableViewCell.identifier,
                for: indexPath) as? BSDMessageTableViewCell
        else { return UITableViewCell() }
        
        cell.populateView(author: author, message: message)
        return cell
    }
    
    func tableView(_ tableView: UITableView, myMessageCellForRowAt indexPath: IndexPath, with author: String, and message: String) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(
                withIdentifier: BSDMyMessageTableViewCell.identifier,
                for: indexPath) as? BSDMyMessageTableViewCell
        else { return UITableViewCell() }
        
        cell.populateView(author: author, message: message)
        return cell
    }
    
}
