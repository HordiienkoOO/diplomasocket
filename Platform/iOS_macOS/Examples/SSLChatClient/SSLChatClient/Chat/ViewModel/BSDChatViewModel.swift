//
//  BSDChatViewModel.swift
//  SSLChatClient
//
//  Created by Oleksandr on 18.10.2020.
//

import SSLSockets

typealias BSDHandleMessageAction = (() -> Void)

enum BSDChatCellType {
    case action(String)
    case message(String, String)
    case myMessage(String, String)
}

class BSDChatViewModel {
    
    // MARK: - Properties
    private var nickname: String
    private var socket: SSLClientSocket?
    private var messages: [BSDChatMessage]
    private var handleMessageAction: BSDHandleMessageAction
    
    // MARK: - Initializers
    init (nickname: String,
          handleMessageAction: @escaping BSDHandleMessageAction)
    {
        self.messages = []
        self.nickname = nickname
        self.handleMessageAction = handleMessageAction
        
        self.socket = SSLClientSocket(
            address: "127.0.0.1",
            port: 8080,
            andDelegate: self)
        self.socket?.start()
    }
    
    deinit {
        let disconnectMessage = BSDDisconnectMessage(user: currentUser())
        socket?.sendData(disconnectMessage.toJSON())
    }
    
    // MARK: - Public methods
    func send(message: String) {
        let stringMessage = BSDStringMessage(
            user: currentUser(),
            message: message)
        socket?.sendData(stringMessage.toJSON())
    }
    
    func numberOfRows() -> Int {
        return messages.count
    }
    
    func cell(for row: Int) -> BSDChatCellType {
        let message = messages[row]
        
        switch message.messageType {
            case .connect:
                let msg = message as! BSDConnectMessage
                return .action("\(msg.user.nickname) has joined to the chat")
                
            case .disconnect:
                let msg = message as! BSDDisconnectMessage
                return .action("\(msg.user.nickname) has left the chat")
                
            case .stringMessage:
                let msg = message as! BSDStringMessage
                return msg.user.nickname == nickname ?
                    .myMessage(msg.user.nickname, msg.message) :
                    .message(msg.user.nickname, msg.message)
                
            case .p2pRequest: assertionFailure("Unexpected type")
        }
        
        return .action("Something went wrong")
    }
    
}

extension BSDChatViewModel: SSLSocketDelegate {
    
    func currentUser() -> BSDChatUser {
        return BSDChatUser(nickname: nickname)
    }
    
    func didConnect(withDescriptor descriptor: Int) {
        socket?.start()
        let message = BSDConnectMessage(user: currentUser())
        socket?.sendData(message.toJSON())
    }
    
    func didReceiveMessage(_ message: String, fromDescriptor descriptor: Int) {
        if let message = BSDMessagesFactory.message(from: message) {
            messages.append(message)
            
            DispatchQueue.main.async { [weak self] in
                self?.handleMessageAction()
            }
        }
    }
    
}
