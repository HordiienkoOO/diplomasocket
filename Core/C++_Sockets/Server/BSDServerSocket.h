/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#pragma once

#include <string>
#include <thread>
#include <vector>
#include "openssl/ssl.h"

#include "IBSDHandlersManager.h"

namespace FBSD
{
    namespace SOCKET
    {
        class SocketHandler;
        class SocketDelegate;
    
        class ServerSocket : IHandlersManager
        {
        private:
            int _port;
            bool _isReady;
            bool _isRunning;
            SSL_CTX *sslContext;
            int _socketDescriptor;
            SocketDelegate *_delegate;
            std::thread retainedThread;
            std::vector<SocketHandler *> acceptedSockets;
            
        private:
            void waitForConnections();
            
        public:
            void stopSocket();
            bool startSocket();
            int port() const;
            bool isReady() const;
            bool isRunning() const;
            SocketDelegate *delegate();
            bool sendData(const char *data, int descriptor);
            const std::vector<SocketHandler *> getAcceptedSockets();
            
        public:
            void didStopHandler(SocketHandler *handler) override;
            
        public:
            ServerSocket(int port, SocketDelegate *delegate);
            ~ServerSocket();
            
        };
    }
}
