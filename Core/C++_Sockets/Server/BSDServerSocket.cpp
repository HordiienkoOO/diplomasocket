/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#include "unistd.h"
#include "sys/socket.h"
#include "netinet/in.h"
#include <openssl/err.h>
#include "openssl/ssl.h"

#include "BSDServerSocket.h"
#include "BSDSocketHandler.h"
#include "BSDSigningManager.h"
#include "BSDSocketDelegate.h"
#include "BSDSocketConstants.h"

#pragma mark - Public methods

int FBSD::SOCKET::ServerSocket::port() const
{
    return _port;
}

bool FBSD::SOCKET::ServerSocket::isReady() const
{
    return _isReady;
}

bool FBSD::SOCKET::ServerSocket::isRunning() const
{
    return _isRunning;
}

FBSD::SOCKET::SocketDelegate *FBSD::SOCKET::ServerSocket::delegate()
{
    return _delegate;
}

const std::vector<FBSD::SOCKET::SocketHandler *> FBSD::SOCKET::ServerSocket::getAcceptedSockets()
{
    return acceptedSockets;
}

bool FBSD::SOCKET::ServerSocket::startSocket() {
    if (_isRunning)
    {
        return true;
    }
    
    if (sslContext)
    {
        _isRunning = true;
        retainedThread = std::thread(&FBSD::SOCKET::ServerSocket::waitForConnections, this);
        return true;
    }
    
    _isReady = false;
    return false;
}

void FBSD::SOCKET::ServerSocket::stopSocket()
{
    if (!_isRunning) return;

    _isRunning = false;
    if (sslContext && SSL_CTX_ct_is_enabled(sslContext))
    {
        SSL_CTX_free(sslContext);
    }

    if (close(_socketDescriptor) == FAIL_CODE)
    {
    }
    else
    {
    }

    long size = acceptedSockets.size();
    
    for (long i = size-1; i >= 0; i--)
    {
        FBSD::SOCKET::SocketHandler *handler = acceptedSockets.at(i);
        handler->stopHandling();
        if (handler) delete handler;
    }

    if (retainedThread.joinable()) retainedThread.join();
}

bool FBSD::SOCKET::ServerSocket::sendData(const char *data, int descriptor) {
    for (FBSD::SOCKET::SocketHandler *handler : acceptedSockets)
    {
        if (handler->descriptor() == descriptor)
        {
            handler->send(data);
            return true;
        }
    }

    return false;
}

#pragma mark - Private methods

void FBSD::SOCKET::ServerSocket::waitForConnections()
{
    _isRunning = true;
    while (_isRunning)
    {
        int acceptedSocket = accept(_socketDescriptor, NULL, NULL);
        if (acceptedSocket >= 0) {
            SSL *acceptedSSL = SSL_new(sslContext);
            SSL_set_fd(acceptedSSL, acceptedSocket);
            
            if (SSL_accept(acceptedSSL) <= 0)
            {
                unsigned long errorCode = ERR_get_error();
            }
            else
            {
                FBSD::SOCKET::SocketHandler *newSocketHandler = new FBSD::SOCKET::SocketHandler(acceptedSSL, acceptedSocket, _delegate, this);
                newSocketHandler->startHandling();
                acceptedSockets.push_back(newSocketHandler);
            }
        }
        else
        {
        }
    }
}

#pragma mark - IHandlersManager confirming
void FBSD::SOCKET::ServerSocket::didStopHandler(FBSD::SOCKET::SocketHandler *handler)
{
    ptrdiff_t idx = find(acceptedSockets.begin(), acceptedSockets.end(), handler) - acceptedSockets.begin();
    if (idx < acceptedSockets.size())
    {
        acceptedSockets.erase(acceptedSockets.begin() + idx);
    }
    else
    {
    }
}

#pragma mark - Lifecycle

FBSD::SOCKET::ServerSocket::ServerSocket(int port, FBSD::SOCKET::SocketDelegate *delegate) {
    _port = port;
    _isReady = true;
    _isRunning = false;
    this->_delegate = delegate;
    FBSD::MANAGEMENT::SigningManager::sharedInstance()->initSSLLibrary();
    
    bool flag = true;
    sslContext = FBSD::MANAGEMENT::SigningManager::sharedInstance()->generateContext();

    _socketDescriptor = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (_socketDescriptor == FAIL_CODE)
    {
        flag = false;
    }
    else
    {
    }
    
    struct sockaddr_in sock_addr;
    memset(&sock_addr, 0, sizeof(sock_addr));
#ifndef ANDROID
    sock_addr.sin_len = sizeof(sock_addr);
#endif
    sock_addr.sin_family = PF_INET;
    sock_addr.sin_port = htons(port);
    sock_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    
    if (bind(_socketDescriptor, (struct sockaddr *)&sock_addr, sizeof(sock_addr)) == FAIL_CODE)
    {
        flag = false;
    }
    else
    {

    }
    
    if (listen(_socketDescriptor, 10) == FAIL_CODE)
    {
        flag = false;
    }
    else
    {
    }
    
    if (!flag)
    {
        close(_socketDescriptor);
        _isReady = false;
    }
    else
    {
    }
}

FBSD::SOCKET::ServerSocket::~ServerSocket()
{
    this->stopSocket();
}
