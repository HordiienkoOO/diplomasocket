/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#pragma once

#include <vector>
#include <thread>
#include <stdio.h>
#include "openssl/ssl.h"

namespace FBSD
{
    namespace SOCKET
    {
        class SocketDelegate;
        class IHandlersManager;
    
        class SocketHandler
        {
            private:
                SSL *ssl;
                int _descriptor;
                bool _isHandling;
                std::thread retainedThread;
                SocketDelegate *delegate;
                IHandlersManager *manager;
                
                void startReading();
                char *readData();
                
            public:
                const SSL *getSSL();
                bool isHandling();
                void startHandling();
                void stopHandling();
                int descriptor() const;
                bool send(const char *data);
                
                SocketHandler(SSL* ssl, int descriptor, SocketDelegate *delegate, IHandlersManager *manager);
                ~SocketHandler();
        };
    }
}
