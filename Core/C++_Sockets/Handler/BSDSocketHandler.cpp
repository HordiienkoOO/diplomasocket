/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#include "unistd.h"
#include <sys/socket.h>
#include <openssl/err.h>

#include "BSDSocketHandler.h"
#include "BSDSocketDelegate.h"
#include "BSDSocketConstants.h"
#include "IBSDHandlersManager.h"

#pragma mark - Helpers

int getLength(std::string data) {
    std::string clearDataLength;
    
    for (int i = 0; i<data.length(); i++)
    {
        if (data[i] >= '0' && data[i] <= '9')
        {
            clearDataLength += data[i];
        }
    }
    
    return std::stoi(clearDataLength);
}

#pragma mark - Public methods

int FBSD::SOCKET::SocketHandler::descriptor() const
{
    return _descriptor;
}

const SSL *FBSD::SOCKET::SocketHandler::getSSL()
{
    return ssl;
}

bool FBSD::SOCKET::SocketHandler::isHandling()
{
    return _isHandling;
}

void FBSD::SOCKET::SocketHandler::startHandling()
{
    if (_isHandling)  return;
    
    _isHandling = true;
    retainedThread = std::thread(&FBSD::SOCKET::SocketHandler::startReading, this);
}

void FBSD::SOCKET::SocketHandler::stopHandling() {
    if (!_isHandling) return;
    
    _isHandling = false;
    
    if (shutdown(_descriptor, SHUT_RDWR) == FAIL_CODE)
    {
    }
    
    if (close(_descriptor) == FAIL_CODE)
    {
    }
    
    SSL_shutdown(ssl);
    SSL_free(ssl);
    if (retainedThread.joinable()) retainedThread.join();
    if (manager) manager->didStopHandler(this);
}

bool FBSD::SOCKET::SocketHandler::send(const char *data) {
    if (!_isHandling) return false;
    
    int initialize[CharSize]; initialize[FirstElementIndex] = { STXSymbolCode };
    int separator[CharSize]; separator[FirstElementIndex] = { EOTSymbolCode };
    int data_length=(int)strlen(data);
    int target_length=snprintf(NULL, 0, "%d", data_length);
    char *data_length_char = (char *)malloc(target_length+CharSize);
    snprintf(data_length_char, target_length+CharSize, "%d", data_length);
    int ele_count=(int)strlen(data_length_char);
    int *size_buff=(int*)malloc(ele_count*sizeof(int));
    
    for (int counter = 0; counter < ele_count; counter++)
    {
        size_buff[counter]=(int)data_length_char[counter];
    }
    int packet_length = sizeof(STXSymbol) + ele_count + sizeof(EOTSymbol) + (int)strlen(data);
    uint8_t *packet=(uint8_t *)malloc(packet_length * sizeof(uint8_t));
    memcpy(&packet[FirstElementIndex], initialize, sizeof(STXSymbol));
    
    for (int counter = 0; counter < ele_count; counter++)
    {
        memcpy(&packet[counter + CharSize], &size_buff[counter], sizeof(EOTSymbol));
    }
    memcpy(&packet[sizeof(STXSymbol) + ele_count], separator, sizeof(EOTSymbol));
    memcpy(&packet[sizeof(STXSymbol) + ele_count + sizeof(EOTSymbol)], data, strlen(data));
    int len = (int)SSL_write(ssl, packet, packet_length);
    
    free(packet);
    free(size_buff);
    free(data_length_char);
    
    if (len <= 0)
    {
        int errorCode = SSL_get_error(ssl, len);
        return false;
    }
   
    return true;
}

void FBSD::SOCKET::SocketHandler::startReading() {
    while (_isHandling)
    {
        char buf[BufSize];
        int bytes = (int)SSL_read(ssl, &buf, BufSize);
        
        if (bytes > 0)
        {
            if ((int)*buf == STXSymbolCode)
            {
                char *receivedData = readData();
                if (delegate) delegate->didReceiveMessage(receivedData, _descriptor);
                free(receivedData);
            }
        }
        
        else if (bytes < 0)
        {
            int errorCode = SSL_get_error(ssl, bytes);
        }
        
        else if (bytes == 0)
        {
            break;
        }
    }
}

char *FBSD::SOCKET::SocketHandler::readData() {
    char *data_buff;
    std::string buff_length;
    char buf[BufSize];
    SSL_read(ssl, &buf, BufSize);
    
    while ((int)*buf != EOTSymbolCode)
    {
        buff_length.append(CharSize, (char)(int)*buf);
        SSL_read(ssl, &buf, BufSize);
    }
    
    int data_length = getLength(buff_length);
    data_buff=(char *)malloc(data_length*sizeof(char));
    ssize_t byte_read = 0;
    ssize_t byte_offset = 0;
    
    while (byte_offset<data_length)
    {
        byte_read = SSL_read(ssl, data_buff+byte_offset, BufferSize);
        byte_offset+=byte_read;
        
        if(byte_read < BufferSize && byte_offset >= data_length)
        {
            data_buff[byte_offset] = NullTerminalSymbol;
            byte_offset += CharSize;
        }
    }

    return data_buff;
}

#pragma mark - Lifecycle

FBSD::SOCKET::SocketHandler::SocketHandler(
    SSL* ssl,
    int descriptor,
    FBSD::SOCKET::SocketDelegate *delegate,
    FBSD::SOCKET::IHandlersManager *manager)
{
    _isHandling = false;
    this->ssl = ssl;
    this->manager = manager;
    this->delegate = delegate;
    this->_descriptor = descriptor;
}

FBSD::SOCKET::SocketHandler::~SocketHandler()
{
}
