/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#pragma once

#include <memory>

namespace FBSD
{
    namespace SOCKET
    {
        class SocketHandler;
    
        class IHandlersManager
        {
        public:
            virtual void didStopHandler(SocketHandler *handler) = 0;
            
        };
    }
}
