/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#pragma once

#include <thread>
#include "openssl/ssl.h"

namespace FBSD
{
    namespace MANAGEMENT
    {
        class SigningManager
        {
        private:
            std::mutex mtxLibrary;
            bool isSSLLibraryInited;
            
            std::string keysFolder;
            bool signContext(SSL_CTX *ctx);
            EVP_PKEY *generate_key();
            bool write_to_disk(EVP_PKEY *pkey, X509 *x509);
            X509 *generate_x509(EVP_PKEY *pkey,
                                const char *country,
                                const char *state,
                                const char *location,
                                const char *organization,
                                const char *organizationUnit,
                                const char *commonName,
                                const char *emailAddress);

            static std::mutex mtxCert;
            static std::mutex mtxSingletone;
            static bool isCertificateGenerated;
            static SigningManager *_sharedInstance;
            
            SigningManager();
            
        public:
            void initSSLLibrary();
            SSL_CTX *generateContext();
            
            static SigningManager *sharedInstance();
            void setKeysFolder(const char *path);
            void configure(const char *country,
                           const char *state,
                           const char *location,
                           const char *organization,
                           const char *organizationUnit,
                           const char *commonName,
                           const char *emailAddress);
        
        };
    }
}
