/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#include <string>
#include <openssl/err.h>

#include "BSDSigningManager.h"

#pragma mark - Helpers
const char *pathForFile(const char *fileName, const char *folder)
{
    std::string strignPath = std::string(folder);
    strignPath.append("/");
    strignPath.append(fileName);
    
    char* ptr = new char[strignPath.size() + 1];
    strcpy(ptr, strignPath.c_str());
    return ptr;
}

#pragma mark - Methods
bool FBSD::MANAGEMENT::SigningManager::signContext(SSL_CTX *ctx)
{
    SSL_CTX_set_ecdh_auto(ctx, 1);
    
    const char *certPath = pathForFile("cert.pem", keysFolder.c_str());
    const char *keyPath = pathForFile("key.pem", keysFolder.c_str());
    
    if (SSL_CTX_use_certificate_file(ctx, certPath, SSL_FILETYPE_PEM) <= 0)
    {
        unsigned long errorCode = ERR_get_error();
        return false;
    }
    if (SSL_CTX_use_PrivateKey_file(ctx, keyPath, SSL_FILETYPE_PEM) <= 0 )
    {
        unsigned long errorCode = ERR_get_error();
        return false;
    }

    return true;
}

EVP_PKEY *FBSD::MANAGEMENT::SigningManager::generate_key()
{
    EVP_PKEY *pkey = EVP_PKEY_new();
    if (!pkey)
    {
        return NULL;
    }

    RSA *rsa = RSA_new();
    BIGNUM *bn = BN_new();
    BN_set_word(bn, RSA_F4);
    int keyResp = RSA_generate_key_ex(rsa, 2048, bn, NULL);
    if (keyResp != 1)
    {
    }

    if (!EVP_PKEY_assign_RSA(pkey, rsa))
    {
        EVP_PKEY_free(pkey);
        return NULL;
    }

    return pkey;
}

X509 *FBSD::MANAGEMENT::SigningManager::generate_x509(EVP_PKEY *pkey,
                                                     const char *country,
                                                     const char *state,
                                                     const char *location,
                                                     const char *organization,
                                                     const char *organizationUnit,
                                                     const char *commonName,
                                                     const char *emailAddress)
{
    X509 *x509 = X509_new();
    if (!x509)
    {
        return NULL;
    }
    
    ASN1_INTEGER_set(X509_get_serialNumber(x509), 1);
    X509_gmtime_adj(X509_get_notBefore(x509), 0);
    X509_gmtime_adj(X509_get_notAfter(x509), 31536000L);
    X509_set_pubkey(x509, pkey);
    X509_NAME *name = X509_get_subject_name(x509);
    X509_NAME_add_entry_by_txt(name, "C",  MBSTRING_ASC, (unsigned char *)country, -1, -1, 0);
    X509_NAME_add_entry_by_txt(name, "ST", MBSTRING_ASC, (unsigned char *)state, -1, -1, 0);
    X509_NAME_add_entry_by_txt(name, "L",  MBSTRING_ASC, (unsigned char *)location, -1, -1, 0);
    X509_NAME_add_entry_by_txt(name, "O",  MBSTRING_ASC, (unsigned char *)organization, -1, -1, 0);
    X509_NAME_add_entry_by_txt(name, "OU", MBSTRING_ASC, (unsigned char *)organizationUnit, -1, -1, 0);
    X509_NAME_add_entry_by_txt(name, "CN", MBSTRING_ASC, (unsigned char *)commonName, -1, -1, 0);
    X509_NAME_add_entry_by_txt(name, "emailAddress", MBSTRING_ASC, (unsigned char *)emailAddress, -1, -1, 0);
    X509_set_issuer_name(x509, name);

    if (!X509_sign(x509, pkey, EVP_sha1()))
    {
        X509_free(x509);
        return NULL;
    }
    
    return x509;
}

bool FBSD::MANAGEMENT::SigningManager::write_to_disk(EVP_PKEY *pkey, X509 *x509)
{
    const char *keyPath = pathForFile("key.pem", keysFolder.c_str());
    FILE * pkey_file = fopen(keyPath, "wb");
    if (!pkey_file)
    {
        return false;
    }
    
    bool ret = PEM_write_PrivateKey(pkey_file, pkey, NULL, NULL, 0, NULL, NULL);
    fclose(pkey_file);
    if (!ret)
    {
        return false;
    }
    
    const char *certPath = pathForFile("cert.pem", keysFolder.c_str());
    FILE * x509_file = fopen(certPath, "wb");
    if (!x509_file) {
        return false;
    }
    
    ret = PEM_write_X509(x509_file, x509);
    fclose(x509_file);
    if (!ret)
    {
        return false;
    }

    return true;
}

void FBSD::MANAGEMENT::SigningManager::initSSLLibrary()
{
    mtxLibrary.lock();
    
    if (isSSLLibraryInited)
    {
        mtxLibrary.unlock();
        return;
    }

    SSL_library_init();
    SSLeay_add_ssl_algorithms();
    SSL_load_error_strings();
    
    isSSLLibraryInited = true;
    mtxLibrary.unlock();
}

SSL_CTX *FBSD::MANAGEMENT::SigningManager::generateContext()
{
    FBSD::MANAGEMENT::SigningManager::mtxCert.lock();
    
    if (!FBSD::MANAGEMENT::SigningManager::isCertificateGenerated)
    {
        throw std::invalid_argument("SSLSocketsManager is not configured. To use SSLServerSocket you should do that.");
    }
    FBSD::MANAGEMENT::SigningManager::mtxCert.unlock();
    
    SSL_CTX *ctx = SSL_CTX_new(SSLv23_server_method());
    if (ctx && signContext(ctx))
    {
        return ctx;
    }
    else
    {
        unsigned long errorCode = ERR_get_error();
        return NULL;
    }
}

#pragma mark - Static properties
std::mutex FBSD::MANAGEMENT::SigningManager::mtxCert;
std::mutex FBSD::MANAGEMENT::SigningManager::mtxSingletone;
bool FBSD::MANAGEMENT::SigningManager::isCertificateGenerated = false;
FBSD::MANAGEMENT::SigningManager *FBSD::MANAGEMENT::SigningManager::_sharedInstance = NULL;

#pragma mark - Static methods
FBSD::MANAGEMENT::SigningManager *FBSD::MANAGEMENT::SigningManager::sharedInstance()
{
    mtxSingletone.lock();
    if (_sharedInstance)
    {
        mtxSingletone.unlock();
        return _sharedInstance;
    }
    _sharedInstance = new SigningManager();
    mtxSingletone.unlock();
    return _sharedInstance;
}

void FBSD::MANAGEMENT::SigningManager::setKeysFolder(const char *path)
{
    keysFolder = std::string(path);
}

void FBSD::MANAGEMENT::SigningManager::configure(const char *country,
                                  const char *state,
                                  const char *location,
                                  const char *organization,
                                  const char *organizationUnit,
                                  const char *commonName,
                                  const char *emailAddress)
{
    
    if (isCertificateGenerated) return;
    mtxCert.lock();
    generate_key();
    EVP_PKEY * pkey = generate_key();
    X509 *x509 = generate_x509(pkey, country, state, location, organization, organizationUnit, commonName, emailAddress);
    write_to_disk(pkey, x509);
    EVP_PKEY_free(pkey);
    X509_free(x509);
    isCertificateGenerated = true;
    mtxCert.unlock();
}

#pragma mark - Constructor
FBSD::MANAGEMENT::SigningManager::SigningManager() {
    isSSLLibraryInited = false;
}
