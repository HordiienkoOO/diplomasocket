/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#pragma once

#include <memory>
#include "openssl/ssl.h"

namespace FBSD
{
    namespace SOCKET
    {
        class SocketDelegate
        {
        public:
            void *platformDelegate;
            void didConnect(int descriptor);
            void didReceiveMessage(char *message, int descriptor);
            
        public:
            SocketDelegate(void *objcDelegate);
            ~SocketDelegate();
            
        };
    }
}
