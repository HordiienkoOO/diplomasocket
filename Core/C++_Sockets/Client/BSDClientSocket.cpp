/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#include "unistd.h"
#include "arpa/inet.h"
#include "netinet/in.h"
#include "sys/socket.h"

#include "BSDClientSocket.h"
#include "BSDSocketHandler.h"
#include "BSDSigningManager.h"
#include "BSDSocketDelegate.h"
#include "BSDSocketConstants.h"

#pragma mark - Public methods

int FBSD::SOCKET::ClientSocket::port() const
{
    return _port;
}

std::string FBSD::SOCKET::ClientSocket::address() const
{
    return _address;
}

bool FBSD::SOCKET::ClientSocket::isReady() const
{
    return _isReady;;
}

bool FBSD::SOCKET::ClientSocket::isRunning() const
{
    return _isRunning;
}

bool FBSD::SOCKET::ClientSocket::startSocket()
{
    if (_isRunning)
    {
        return true;
    }
    
    if (!_isReady)
    {
        return false;
    }
    
    handler->startHandling();
    _isRunning = true;
    delegate->didConnect(handler->descriptor());
    return true;
}

void FBSD::SOCKET::ClientSocket::stopSocket()
{
    _isRunning = false;
    if (handler)
    {
        handler->stopHandling();
        delete handler;
        handler = NULL;
    }
}

bool FBSD::SOCKET::ClientSocket::sendData(const char *data)
{
    if (!_isRunning)
    {
        return false;
    }

    return handler->send(data);
}

#pragma mark - Constructor
FBSD::SOCKET::ClientSocket::ClientSocket(std::string &address, int port, FBSD::SOCKET::SocketDelegate *delegate)
{
    _port = port;
    _isReady = true;
    _isRunning = false;
    _address = address;
    this->delegate = delegate;
    FBSD::MANAGEMENT::SigningManager::sharedInstance()->initSSLLibrary();
    
    
    int descriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (descriptor == FAIL_CODE)
    {
        _isReady = false;
    }
    else
    {
    }
    
    struct sockaddr_in sock_addr;
    memset(&sock_addr, 0, sizeof(sock_addr));
#ifndef ANDROID
    sock_addr.sin_len = sizeof(sock_addr);
#endif
    sock_addr.sin_family = AF_INET;
    sock_addr.sin_port = htons(port);
    _isReady = (bool)inet_pton(AF_INET, address.c_str(), &sock_addr.sin_addr);
    
    if (connect(descriptor, (struct sockaddr *)&sock_addr, sizeof(sock_addr)) == FAIL_CODE)
    {
        close(descriptor);
        _isReady = false;
    }
    else
    {
    }
    
    SSL_CTX *ctx = SSL_CTX_new(TLS_client_method());
    
    SSL* ssl = SSL_new(ctx);
    if (!ssl)
    {
        _isReady = false;
    }
    else
    {
    }
    
    SSL_set_fd(ssl, descriptor);
    int err = SSL_connect(ssl);
    if (err <= 0)
    {
        _isReady = false;
        int errorCode = SSL_get_error(ssl, err);
    }
    else
    {
        handler = new FBSD::SOCKET::SocketHandler(ssl, descriptor, delegate, NULL);
    }
}

#pragma mark - Destructor

FBSD::SOCKET::ClientSocket::~ClientSocket()
{
    this->stopSocket();
}
