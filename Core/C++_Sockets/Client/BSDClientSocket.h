/*
 * (c) 2020 cahebu4. All rights reserved.
 */

#include <string>

#pragma once

namespace FBSD
{
    namespace SOCKET
    {
        class SocketHandler;
        class SocketDelegate;
    
        class ClientSocket
        {
        private:
            int _port;
            bool _isReady;
            bool _isRunning;
            std::string _address;
            SocketHandler *handler;
            SocketDelegate *delegate;
            
        public:
            int port() const;
            void stopSocket();
            bool startSocket();
            bool isReady() const;
            bool isRunning() const;
            std::string address() const;
            bool sendData(const char *data);
            
        public:
            ClientSocket(std::string &address, int port, FBSD::SOCKET::SocketDelegate *delegate);
            ~ClientSocket();
            
        };
    }
}
